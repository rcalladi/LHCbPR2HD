import os
import re
import json
from BaseHandler import BaseHandler


class RadLengthHandler(BaseHandler):
	
	def checkFile(self,file_):
		
	    if not os.path.exists(file_):
		raise Exception("File %s does not exist" % file_) 
		return False
	    else:
		return True

	def readTable(self,file_,label=""):

	    self.checkFile(file_)
	    table = []

	    with open(file_, mode='r') as f:
		 lines = f.readlines()
		 for line in lines[1:]:
		     if not line:
			raise Exception("Failed to read data file %s" % file_)
		     x = re.findall(r"(.+?)\s+&\s([\d.]+)\s\\pm\s([\d.]+)",line)[0]
		     rad_length = x[0]+"_radlength%s" % label,float(x[1])
		     rad_length_err = x[0]+"_radlength%s_err" % label,float(x[2])
	             table.append((x[0],float(x[1]),float(x[2])))
	    self.saveString("RADLENGTH_TABLE_%s" % label, json.dumps(table))


	def __init__(self):
		super(self.__class__,self).__init__()


	def collectResults(self,directory):
	
		subdir = 'Rad_length/root_files/'

		radroot = ['Rad_merged.root','Rad_velo_z.root']
 
		filenames = [os.path.join(directory,subdir,i) for i in radroot] 

		for i,filename in enumerate(filenames):
			if self.checkFile(filename):
				self.saveFile(radroot[i],filename)	
		
	
		Radlogfilename = os.path.join(directory,'Rad_length/data_tables/radLengthOut.txt')
		Interlogfilename = os.path.join(directory,'Rad_length/data_tables/interLengthOut.txt')

		self.checkFile(Radlogfilename)
		self.checkFile(Interlogfilename)

		self.readTable(Radlogfilename,"RadLength")
		self.readTable(Interlogfilename,"InterLength")

